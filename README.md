# Network Monitoring with ELK Stack



## Project Description

In this project we tried to integrate a tool like Autofocus to and exisitng monitoring/analysis solution like the ELK stack.
ELk stands for Elasticsearch, Logstash and Kibana. This stack can overwlmly imporve some aspects of autofocus that are not really appealing or do not reach certain standards to be a tool in the market.

In our project we have seen that integratin Autofocus, which is just a concept tool, to ELk, is  far from trivial. We tried to show the power of ELK and its potential to analyse heavy hitter patterns or perfom traffic analysis and monitoring.

![image](./architecture.png)

## Code

Since our project is more a Infrastructure projec, the only code 
provided is the configuration files for each component of the project.

## Testing

To test the stack, you can send flows with your netflow generator of choice to 40.113.97.115:2055, and then visualise them at Kibana in 40.113.97.115:5601. The dashboard available is called TMA. Credentials required for Kibana are:

* username: elastic
* password: Tm4pr0j3cts3rv3r

After some seconds you should start seeing the charts update with values. 

Please do not send too many flows, since the machine resources are low, and many flows could freex the machine.


